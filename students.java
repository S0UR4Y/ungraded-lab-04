public class students{
    public int amountLearned;
    private int grade;
    private int score;
    private int age;
   //constructor
    public students( int grade, int score){
        this.grade=grade;
        this.score=score;
        this.age=3;
        this.amountLearned=0;
    }
    public void learn(int amountStudied){
        this.amountLearned+=amountStudied;
        
    }
    public int getGrade(){
        return grade;
    }
    public int getAge(){
        return age;
    }
    public int getscore(){
        return score;
    }
    public void setGrade(int grade){
        this.grade=grade;
    }
    public void setscore(int score){
        this.score=score;
    }
    public void setage(int age){
        this.age=age;
    }
}